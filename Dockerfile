FROM gcr.io/google.com/cloudsdktool/cloud-sdk:alpine
RUN apk --update add curl
RUN gcloud components install cloud-datastore-emulator kubectl
